@extends('layouts.app')

@section('content')
	<div class="mt-3">
		<a href="/posts" class="card-link">View All Posts</a>
	</div>
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			
		</div>
			<div>
				@if(Auth::id() != $post->user_id)
					<form action="/posts/{{$post->id}}/like" class="d-inline" method="post">
						@method('PUT')
						@csrf
						@if($post->likes->contains('user_id', Auth::id()))
							<button type="submit" class="btn btn-primary mb-3">{{ $post->likes->count() }} Like</button>
						@else
							<button type="submit" class="btn btn-success mb-3">{{ $post->likes->count() }} Like</button>
						@endif
					</form>
				@endif
			
				<button class="btn btn-success mb-3" id="toggleButton" onclick="toggleContent()">{{ $post->comments->count() }} Comment</button>
				
			</div>
	</div>

	<div>
		<form id="commentForm" style="display: none;" action="/posts/{{$post->id}}/comment"  method="post">
			@method('POST')
			@csrf
			<textarea id="content"  class="form-control my-2" id="content" name="content" rows="3" required></textarea>
			<button class="btn btn-primary my-2" type="submit">Save</button>
			<button class="btn btn-danger my-2" id="toggleButton" onclick="toggleContent()">Cancel</button>
		</form>
	</div>


	<div>
	@if(count($post->comments) > 0)
			@foreach($post->comments as $comment)
    			<div class="card text-left mt-1">
                    <div class="card-body">
                        <h3 class="card-title mb-3">{{$comment->content}}</h4>
                        <h6 class="card-text mb-3">Author: {{$comment->user->name}}</h6>
                        <p class="card-subtitle mb-3 text-muted">Created at: {{$comment->created_at}}</p>
                    </div>
                </div>
			@endforeach
	@else 
		<!-- This will show only if there are no posts that are created yet. -->
		<div>
			<h2>There are no comments to show!</h2>
		</div>
	@endif
	</div>


	<script>
		function toggleContent() {
			var form = document.getElementById("commentForm");
			if (form.style.display === "none" && {{Auth::id()}} != null) {
				form.style.display = "block";
			} else {
				form.style.display = "none";
			}
		}
	</script>

@endsection